﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Pipe.EIA99xtHR.Data.Core;
using System.ComponentModel.DataAnnotations; 

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class User : TransactionBase
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string HRISCategory { get; set; }

        public virtual ICollection<UserGroup> MyGroups { get; set; }
    }
}
