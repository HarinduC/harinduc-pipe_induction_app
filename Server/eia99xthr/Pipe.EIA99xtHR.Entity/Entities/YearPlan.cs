﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class YearPlan : TransactionBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
