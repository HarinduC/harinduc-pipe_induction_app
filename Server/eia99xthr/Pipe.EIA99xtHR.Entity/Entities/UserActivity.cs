﻿using Pipe.EIA99xtHR.Data.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class UserActivity : TransactionBase
    {
        public int UserId { get; set; }
        public int? AnnouncementId { get; set; }
        public int? DocumentId { get; set; }
        public int? YearPlanId { get; set; }
        public int? FeedbackId { get; set; }
        public string Activity { get; set; }


        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("AnnouncementId")]
        public virtual Announcement Announcement { get; set; }

        [ForeignKey("DocumentId")]
        public virtual Document Document { get; set; }

        [ForeignKey("YearPlanId")]
        public virtual YearPlan YearPlan { get; set; }

        [ForeignKey("FeedbackId")]
        public virtual UserFeedback Feedback { get; set; }
    }
}
