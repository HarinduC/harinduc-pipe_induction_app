﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class UserFeedback : TransactionBase
    {
        public int OwnerId { get; set; }        
        public DateTime SentTime { get; set; }
        public string Feedback { get; set; }

        [ForeignKey("OwnerId")]
        public virtual User Owner { get; set; }
    }
}
