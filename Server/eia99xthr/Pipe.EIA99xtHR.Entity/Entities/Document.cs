﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class Document : TransactionBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Content { get; set; }
    }
}
