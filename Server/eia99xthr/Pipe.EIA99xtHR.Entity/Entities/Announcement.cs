﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class Announcement : TransactionBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedTime { get; set; }
        public int TargetUserGroupId { get; set; }

        [ForeignKey("CreatedById")]
        public virtual User CreatedBy { get; set; }

        [ForeignKey("TargetUserGroupId")]
        public virtual UserGroup TargetUserGroup { get; set; }
    }
}
