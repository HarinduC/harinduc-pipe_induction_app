﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class Setting : TransactionBase
    {
        public string Owner { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
