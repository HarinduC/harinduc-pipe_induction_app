﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Pipe.EIA99xtHR.Data.Core;
using System.ComponentModel.DataAnnotations; 

namespace Pipe.EIA99xtHR.Entity.Entities
{
    public class TransactionBase : IIdentifier
    {
        public int Id { get; set; }        
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
