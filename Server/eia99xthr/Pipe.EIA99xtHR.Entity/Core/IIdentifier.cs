﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Data.Core
{
    public interface IIdentifier
    {
        int Id { get; set; }

        // Only Id will be used as an unique identifier
        // Guid GUID { get; set; }

        byte[] RowVersion { get; set; }
    }
}
