﻿using Pipe.EIA99xtHR.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Data.Seed
{
    class SeedSampleData : ISeed
    {
        public void Seed(DatabaseContext context)
        {            
            context.Database.CreateIfNotExists();      
            
            // Add sample data here
            
        }
    }
}
