﻿using Pipe.EIA99xtHR.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Data.Seed
{
    interface ISeed
    {
        void Seed(DatabaseContext context);
    }
}
