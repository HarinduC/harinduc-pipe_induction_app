﻿using Pipe.EIA99xtHR.Data.Core;
using Pipe.EIA99xtHR.Entity;
using Pipe.EIA99xtHR.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Data
{
    class DatabaseContext : BaseContext
    {
        public DatabaseContext(string connectionStringName) :base (connectionStringName)
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            Database.Log = s => LogDbOperations(s); 
        }

        private void LogDbOperations(string s) {
            Debug.Write(s);
        }

        // Configurations
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Users have many groups, groups have many users
            modelBuilder.Entity<UserGroup>().HasMany(g => g.Users).WithMany(u => u.MyGroups);
        }

        // Entities
        public DbSet<User> Users { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<UserFeedback> UserFeedbacks { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<YearPlan> YearPlans { get; set; }        
    }
}
