﻿using Pipe.EIA99xtHR.Data.Core;
using Pipe.EIA99xtHR.Entity;
using Pipe.EIA99xtHR.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Data.Repos
{
    internal class ProductRepository: Repository<UserActivity>
    {
        public ProductRepository(DbContext context) : base(context) { }

        /* override any method to provide custom behaviour */
        //public override Product Update(Product entityToUpdate) { return null; }
    }
}
