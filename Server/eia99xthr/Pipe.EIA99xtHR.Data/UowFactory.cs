﻿using Pipe.EIA99xtHR.Data.Core;
using Pipe.EIA99xtHR.Data.Repos;
using Pipe.EIA99xtHR.Data.Seed;
using Pipe.EIA99xtHR.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipe.EIA99xtHR.Data
{
    public static class UowFactory
    {
        static UowFactory()
        {
            Database.SetInitializer(new DropIfChanged(new List<ISeed> { 
                new SeedBaseData(), 
                new SeedSampleData()
            }));


        }

        public static IUnitOfWork Create(string connectionStringName)
        {
            var context = new DatabaseContext(connectionStringName);
            var uow = new UnitOfWork(context);
            
            // Sample code
            //register any extended entity repository or custom repositories
            //uow.RegisterEntityRepository<Product>(new ProductRepository(context));

            return uow;
        }
    }
}
