﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pipe.EIA99xtHR.BusinessLogic;
using Pipe.EIA99xtHR.Data;
using Pipe.EIA99xtHR.Data.Core;
using Pipe.EIA99xtHR.Entity;

namespace Pipe.EIA99xtHR.Web.Core
{
    public class UnitOfWorkController : Controller
    {
        protected IUnitOfWork UnitOfWork { get; set; }

        public UnitOfWorkController()
        {
            UnitOfWork = UowFactory.Create("DefaultConnection");
        }

        /// <summary>
        /// Dispose the unit of work on controller disposal
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            UnitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
