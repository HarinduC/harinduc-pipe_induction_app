﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pipe.EIA99xtHR.Web.Startup))]
namespace Pipe.EIA99xtHR.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
