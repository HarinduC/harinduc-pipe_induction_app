﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pipe.EIA99xtHR.Entity.Entities;
using Pipe.EIA99xtHR.Web.Core;

namespace Pipe.EIA99xtHR.Web.Controllers
{
    public class HomeController : UnitOfWorkController
    {
        public ActionResult Index()
        {
            var p = UnitOfWork.GetEntityRepository<User>().GetAll().ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}