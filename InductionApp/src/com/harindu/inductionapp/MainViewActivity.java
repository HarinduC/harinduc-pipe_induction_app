package com.harindu.inductionapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_view);
	}
	
	public void announcement(View view){
		
		Intent i = new Intent(MainViewActivity.this,AnnouncementActivity.class);
		startActivity(i);
		
		
	}
	
	public void feedback(View view){
		
		Intent i = new Intent(MainViewActivity.this,FeedbackActivity.class);
		startActivity(i);
		
	}
	
	public void yearplan(View view){
		
		Intent i = new Intent(MainViewActivity.this,YearPlanActivity.class);
		startActivity(i);
		
	}

	public void document(View view){
		
		Intent i = new Intent(MainViewActivity.this,DocumentActivity.class);
		startActivity(i);
		
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_view, menu);
		return true;
	}

}
