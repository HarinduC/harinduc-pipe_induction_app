package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UserActivityTable {

	public static final String TABLE_USER_ACTIVITY = "userActivity";
	public static final String COLUMN_ID = "activityId";
	public static final String COLUMN_USER_ID = "userId";
	public static final String COLUMN_DOCUMENT_ID ="docId";
	public static final String COLUMN_ANNOUNCEMENT_ID ="announceId";
	public static final String COLUMN_YEAR_ID ="yearId";
	public static final String COLUMN_FEEDBACK_ID = "feedbackId";
	public static final String COLUMN_ACTIVITY ="activity";
	
	private static final String USER_ACTIVITY_CREATE = "create table "+TABLE_USER_ACTIVITY+
			
			"("
			+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_USER_ID+"integer not null,"
			+COLUMN_DOCUMENT_ID+"integer not null,"
			+COLUMN_ANNOUNCEMENT_ID+"integer not null,"
			+COLUMN_YEAR_ID+"integer not null,"
			+COLUMN_FEEDBACK_ID+"integer not null,"
			+COLUMN_ACTIVITY+"integer not null,"
			+" foreign key("+COLUMN_USER_ID+") references User(userId) ,"
			+" foreign key("+COLUMN_DOCUMENT_ID+") references document(docId) ,"
			+" foreign key("+COLUMN_ANNOUNCEMENT_ID+") references announcement(announceId) ,"
			+" foreign key("+COLUMN_YEAR_ID+") references yearPlan(yearId) ,"
			+" foreign key("+COLUMN_FEEDBACK_ID+") references feeback(feedbackId) );";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(USER_ACTIVITY_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(UserActivityTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_ACTIVITY);
		onCreate(database);
	}
}
