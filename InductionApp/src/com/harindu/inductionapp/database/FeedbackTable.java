package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FeedbackTable {

	public static final String TABLE_FEEDBACK = "feeback";
	public static final String COLUMN_ID = "feedbackId";
	public static final String COLUMN_USER_ID ="userId";
	public static final String COLUMN_FEEDBACK = "feedbackDesc";
	public static final String COLUMN_SENT_TIME ="sentTime";
	
	private static final String FEEDBACK_CREATE = "create table"+TABLE_FEEDBACK+
			
			"("
			+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_USER_ID+"integer not null,"
			+COLUMN_FEEDBACK+"text not null,"
			+COLUMN_SENT_TIME+"timestamp not null,"
			+"foreign key("+COLUMN_USER_ID+") references User(id));";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(FEEDBACK_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(FeedbackTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_FEEDBACK);
		onCreate(database);
	}
	
}
