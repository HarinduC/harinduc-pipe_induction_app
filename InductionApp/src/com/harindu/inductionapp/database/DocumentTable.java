package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DocumentTable {

	public static final String TABLE_DOCUMENT ="document";
	public static final String COLUMN_ID = "docId";
	public static final String COLUMN_TITLE ="title";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_CONTENT = "content";
	
	private static final String DOCUMENT_CRETAE = "create table "+TABLE_DOCUMENT+
			
			"("
			+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_TITLE+"text not null,"
			+COLUMN_DESCRIPTION+"text not null,"
			+COLUMN_CONTENT+"text not null,"
			+");";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DOCUMENT_CRETAE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(DocumentTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCUMENT);
		onCreate(database);
	}
			
}
