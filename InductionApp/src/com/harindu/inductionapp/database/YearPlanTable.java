package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class YearPlanTable {

	public static final String TABLE_YEAR_PLAN = "yearPlan";
	public static final String COLUMN_ID = "yearId";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_DESCRIPTION = "description";
	
	private static final String YEAR_PLAN_CREATE = "create table "+ TABLE_YEAR_PLAN + "( "
	
			+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_NAME+"text not null,"
			+COLUMN_DESCRIPTION+"text not null,"
			+");";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(YEAR_PLAN_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(YearPlanTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_YEAR_PLAN);
		onCreate(database);
	}
}
