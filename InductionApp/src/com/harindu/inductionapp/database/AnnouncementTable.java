package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class AnnouncementTable {

	public static final String TABLE_ANNOUNCEMENT = "announcement";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_CREATED_TIME = "createdTime";
	public static final String COLUMN_CREATED_BY = "createdBy";
	public static final String COLUMN_TARGETED_GROUP = "targetGroup";
	
	private static final String ANNOUNCEMENT_CREATE = "create table "+ TABLE_ANNOUNCEMENT + 
			
			"( "
			+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_TITLE+"text not null,"
			+COLUMN_DESCRIPTION+"text not null,"
			+COLUMN_CREATED_TIME+"timestamp not null,"
			+COLUMN_CREATED_BY+"text not null,"
			+COLUMN_TARGETED_GROUP+"text not null,"
			+");";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(ANNOUNCEMENT_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(AnnouncementTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_ANNOUNCEMENT);
		onCreate(database);
	}
}
