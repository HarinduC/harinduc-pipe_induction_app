package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UserUserGroupTable {

	public static final String TABLE_USER_USER_GROUP = "userUserGroup";
	public static final String COLUMN_USER_ID = "userId";
	public static final String COLUMN_GROUP_ID = "groupId";
	
	private static final String USER_USER_GROUP_CREATE = "create table"+TABLE_USER_USER_GROUP+
			
			"("
			+COLUMN_USER_ID+"integer not null,"
			+COLUMN_GROUP_ID+"integer not null,"
			+"primary key("+COLUMN_USER_ID+","+COLUMN_GROUP_ID+"), foreign key("+COLUMN_USER_ID+") references User("+COLUMN_USER_ID+")"+
			
			"foreign key("+COLUMN_GROUP_ID+") references userGroup("+COLUMN_GROUP_ID+") );";
			
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(USER_USER_GROUP_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(UserUserGroupTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_USER_GROUP);
		onCreate(database);
	}
}
