package com.harindu.inductionapp.database;



import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UserTable {

	//Database table for User
	
	public static final String TABLE_USER = "User";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_USER_NAME = "userName";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_CATEGORY = "HRISCategory";
	
	// Database creation sql statement 
	
	private static final String USER_CREATE = "create table" + TABLE_USER + "("
	
			+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_USER_NAME+" text not null,"
			+COLUMN_EMAIL+" text not null,"
			+COLUMN_CATEGORY+" text not null"+
			
			");"; 
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(USER_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(UserTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		onCreate(database);
	}
	
}
