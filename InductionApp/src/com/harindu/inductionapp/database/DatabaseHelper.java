package com.harindu.inductionapp.database;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static DatabaseHelper sInstance;
	private static final String DATABASE_NAME = "pipe.db";
	private static final int DATABASE_VERSION = 1;

	public static DatabaseHelper getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new DatabaseHelper(context.getApplicationContext());
		}
		return sInstance;
	}
	
	
	private DatabaseHelper(Context context) {
		super(context, DATABASE_NAME,null,DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		AnnouncementTable.onCreate(database);
		DocumentTable.onCreate(database);
		FeedbackTable.onCreate(database);
		UserActivityTable.onCreate(database);
		UserGroupTable.onCreate(database);
		UserTable.onCreate(database);
		UserUserGroupTable.onCreate(database);
		YearPlanTable.onCreate(database);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
		AnnouncementTable.onUpgrade(database, oldVersion, newVersion);
		DocumentTable.onUpgrade(database, oldVersion, newVersion);
		FeedbackTable.onUpgrade(database, oldVersion, newVersion);
		UserActivityTable.onUpgrade(database, oldVersion, newVersion);
		UserGroupTable.onUpgrade(database, oldVersion, newVersion);
		UserTable.onUpgrade(database, oldVersion, newVersion);
		UserUserGroupTable.onUpgrade(database, oldVersion, newVersion);
		YearPlanTable.onUpgrade(database, oldVersion, newVersion);
		
	}

	
}
