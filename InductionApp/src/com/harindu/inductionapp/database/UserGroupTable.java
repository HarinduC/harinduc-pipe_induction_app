package com.harindu.inductionapp.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UserGroupTable {

	public static final String TABLE_USER_GROUP ="userGroup";
	public static final String COLUMN_ID = "userGroupId";
	public static final String COLUMN_GROUP = "groupName";
	
	private static final String USER_GROUP_CREATE = "create table "+ TABLE_USER_GROUP + 
			
			"( "+COLUMN_ID+"integer primary key autoincrement,"
			+COLUMN_GROUP+"text not null,"+");";
	
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(USER_GROUP_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(UserGroupTable.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");

		database.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_GROUP);
		onCreate(database);
	}
}
