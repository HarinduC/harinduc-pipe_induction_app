package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.UserUserGroupTable;
import com.harindu.inductionapp.model.UserUserGroup;
import com.harindu.inductionapp.model.UserUserGroupListResultObject;


public class UserUserGroupDataSource {

private static final String TAG = UserUserGroupDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {UserUserGroupTable.COLUMN_USER_ID, 
			
				UserUserGroupTable.COLUMN_GROUP_ID, 
				};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertUserUserGroup(UserUserGroupListResultObject userUserGroupList) throws SQLException{
		
		Log.d(TAG, "Inserting UserUserGroups");
		database.delete(UserUserGroupTable.TABLE_USER_USER_GROUP, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(UserUserGroup userUserGroup : userUserGroupList.getUserUserGroup() ){
				
				ContentValues cv = new ContentValues();
				cv.put(UserUserGroupTable.COLUMN_USER_ID, userUserGroup.getUserId());
				cv.put(UserUserGroupTable.COLUMN_GROUP_ID,userUserGroup.getGroupId());
				
			database.insert(UserUserGroupTable.TABLE_USER_USER_GROUP, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public UserUserGroupListResultObject getUserUserGroups(){
		
		Log.d(TAG, "Getting userUser groups");
		
		UserUserGroupListResultObject userUserGroupListResObj = new UserUserGroupListResultObject();
		ArrayList<UserUserGroup> userUserGroup = new ArrayList<UserUserGroup>();
		userUserGroupListResObj.setUserUserGroup(userUserGroup);
		
		
		Cursor cursor = database.query(UserUserGroupTable.TABLE_USER_USER_GROUP, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			UserUserGroup uug = cursorToUserUserGroup(cursor);
			userUserGroup.add(uug);
			cursor.moveToNext();
		}
		userUserGroupListResObj.setUserUserGroup(userUserGroup);
		cursor.close();
		Log.d(TAG, "UserUserGroups count" + userUserGroup.size());
		return userUserGroupListResObj;
		
	}
	
	private UserUserGroup cursorToUserUserGroup(Cursor cursor){
		
		UserUserGroup ro = new UserUserGroup();
		ro.setUserId(cursor.getString(0));
		ro.setGroupId(cursor.getString(1));
		return ro;
		
		
	}
}
