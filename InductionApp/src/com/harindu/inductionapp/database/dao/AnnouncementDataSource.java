package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import com.harindu.inductionapp.database.AnnouncementTable;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.model.AnnouncementListResultObject;
import com.harindu.inductionapp.model.Announcements;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class AnnouncementDataSource {

	private static final String TAG = AnnouncementDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {AnnouncementTable.COLUMN_ID, 
			
				AnnouncementTable.COLUMN_TITLE, 
				AnnouncementTable.COLUMN_DESCRIPTION,
				AnnouncementTable.COLUMN_CREATED_TIME,
				AnnouncementTable.COLUMN_CREATED_BY,
				AnnouncementTable.COLUMN_TARGETED_GROUP};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertAnnouncements(AnnouncementListResultObject announcementList) throws SQLException{
		
		Log.d(TAG, "Inserting Announcements");
		database.delete(AnnouncementTable.TABLE_ANNOUNCEMENT, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(Announcements announcements : announcementList.getAnnouncements() ){
				
				ContentValues cv = new ContentValues();
				cv.put(AnnouncementTable.COLUMN_TITLE, announcements.getTitle());
				cv.put(AnnouncementTable.COLUMN_DESCRIPTION, announcements.getDescription());
				cv.put(AnnouncementTable.COLUMN_CREATED_TIME,announcements.getCreatedDate());
				cv.put(AnnouncementTable.COLUMN_CREATED_BY, announcements.getCreatedBy());
				cv.put(AnnouncementTable.COLUMN_TARGETED_GROUP, announcements.getTargetGroup());
				
			database.insert(AnnouncementTable.TABLE_ANNOUNCEMENT, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public AnnouncementListResultObject getAnnouncements(){
		
		Log.d(TAG, "Getting announcements");
		
		AnnouncementListResultObject announcementListResObj = new AnnouncementListResultObject();
		ArrayList<Announcements> announcements = new ArrayList<Announcements>();
		announcementListResObj.setAnnouncements(announcements);
		
		Cursor cursor = database.query(AnnouncementTable.TABLE_ANNOUNCEMENT, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Announcements anncnmnts = cursorToAnnouncements(cursor);
			announcements.add(anncnmnts);
			cursor.moveToNext();
		}
		announcementListResObj.setAnnouncements(announcements);
		cursor.close();
		Log.d(TAG, "Announcements count" + announcements.size());
		return announcementListResObj;
		
	}
	
	private Announcements cursorToAnnouncements(Cursor cursor){
		
		Announcements anns = new Announcements();
		anns.setId(cursor.getInt(0));
		anns.setTitle(cursor.getString(1));
		anns.setDescription(cursor.getString(2));
		anns.setCreatedDate(cursor.getString(3));
		anns.setCreatedBy(cursor.getString(4));
		anns.setTargetGroup(cursor.getString(5));
		return anns;
		
		
	}
	
	
	
	
	
	
}
