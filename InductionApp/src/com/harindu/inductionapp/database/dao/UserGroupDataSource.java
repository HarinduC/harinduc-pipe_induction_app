package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.UserGroupTable;
import com.harindu.inductionapp.model.UserGroup;
import com.harindu.inductionapp.model.UserGroupListResultObject;


public class UserGroupDataSource {

	private static final String TAG = UserGroupDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {UserGroupTable.COLUMN_ID, 
			
				UserGroupTable.COLUMN_GROUP, 
				};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertUserGroup(UserGroupListResultObject userGroupList) throws SQLException{
		
		Log.d(TAG, "Inserting UserGroups");
		database.delete(UserGroupTable.TABLE_USER_GROUP, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(UserGroup userGroup : userGroupList.getUserGroup() ){
				
				ContentValues cv = new ContentValues();
				cv.put(UserGroupTable.COLUMN_GROUP, userGroup.getGroupName());
				
			database.insert(UserGroupTable.TABLE_USER_GROUP, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public UserGroupListResultObject getUserGroups(){
		
		Log.d(TAG, "Getting user groups");
		
		UserGroupListResultObject userGroupListResObj = new UserGroupListResultObject();
		ArrayList<UserGroup> userGroup = new ArrayList<UserGroup>();
		userGroupListResObj.setUserGroup(userGroup);
		
		
		Cursor cursor = database.query(UserGroupTable.TABLE_USER_GROUP, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			UserGroup ug = cursorToUserGroup(cursor);
			userGroup.add(ug);
			cursor.moveToNext();
		}
		userGroupListResObj.setUserGroup(userGroup);
		cursor.close();
		Log.d(TAG, "UserGroups count" + userGroup.size());
		return userGroupListResObj;
		
	}
	
	private UserGroup cursorToUserGroup(Cursor cursor){
		
		UserGroup ro = new UserGroup();
		ro.setId(cursor.getInt(0));
		ro.setGroupName(cursor.getString(1));
		return ro;
		
		
	}
}
