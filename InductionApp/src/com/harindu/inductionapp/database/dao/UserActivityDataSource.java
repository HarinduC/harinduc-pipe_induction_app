package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.UserActivityTable;
import com.harindu.inductionapp.model.UserActivity;
import com.harindu.inductionapp.model.UserActivityListResultObject;

public class UserActivityDataSource {

	private static final String TAG = UserActivityDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {UserActivityTable.COLUMN_ID, 
			
				UserActivityTable.COLUMN_USER_ID, 
				UserActivityTable.COLUMN_DOCUMENT_ID,
				UserActivityTable.COLUMN_ANNOUNCEMENT_ID,
				UserActivityTable.COLUMN_YEAR_ID,
				UserActivityTable.COLUMN_FEEDBACK_ID,
				UserActivityTable.COLUMN_ACTIVITY};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertUserActivity(UserActivityListResultObject userActivityList) throws SQLException{
		
		Log.d(TAG, "Inserting UserActivities");
		database.delete(UserActivityTable.TABLE_USER_ACTIVITY, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(UserActivity userActivity : userActivityList.getUserActivity() ){
				
				ContentValues cv = new ContentValues();
				cv.put(UserActivityTable.COLUMN_USER_ID, userActivity.getUserId());
				cv.put(UserActivityTable.COLUMN_DOCUMENT_ID, userActivity.getDocId());
				cv.put(UserActivityTable.COLUMN_ANNOUNCEMENT_ID,userActivity.getAnnId());
				cv.put(UserActivityTable.COLUMN_YEAR_ID, userActivity.getYrId());
				cv.put(UserActivityTable.COLUMN_FEEDBACK_ID, userActivity.getFdId());
				cv.put(UserActivityTable.COLUMN_ACTIVITY, userActivity.getActivity());
				
			database.insert(UserActivityTable.TABLE_USER_ACTIVITY, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public UserActivityListResultObject getUserActivities(){
		
		Log.d(TAG, "Getting user activities");
		
		UserActivityListResultObject userActivityListResObj = new UserActivityListResultObject();
		ArrayList<UserActivity> userActivity = new ArrayList<UserActivity>();
		userActivityListResObj.setUserActivity(userActivity);
		
		
		Cursor cursor = database.query(UserActivityTable.TABLE_USER_ACTIVITY, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			UserActivity ua = cursorToUserActivity(cursor);
			userActivity.add(ua);
			cursor.moveToNext();
		}
		userActivityListResObj.setUserActivity(userActivity);
		cursor.close();
		Log.d(TAG, "UserActivities count" + userActivity.size());
		return userActivityListResObj;
		
	}
	
	private UserActivity cursorToUserActivity(Cursor cursor){
		
		UserActivity ro = new UserActivity();
		ro.setId(cursor.getInt(0));
		ro.setUserId(cursor.getString(1));
		ro.setDocId(cursor.getString(2));
		ro.setAnnId(cursor.getString(3));
		ro.setYrId(cursor.getString(4));
		ro.setFdId(cursor.getString(5));
		ro.setActivity(cursor.getString(6));
		return ro;
		
		
	}
}
