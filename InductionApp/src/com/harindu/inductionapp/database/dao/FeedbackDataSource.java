package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.FeedbackTable;
import com.harindu.inductionapp.model.Feedback;
import com.harindu.inductionapp.model.FeedbackListResultObject;

public class FeedbackDataSource {

private static final String TAG = FeedbackDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {FeedbackTable.COLUMN_ID, 
			
				FeedbackTable.COLUMN_USER_ID, 
				FeedbackTable.COLUMN_FEEDBACK,
				FeedbackTable.COLUMN_SENT_TIME};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertFeedback(FeedbackListResultObject feedbackList) throws SQLException{
		
		Log.d(TAG, "Inserting Announcements");
		database.delete(FeedbackTable.TABLE_FEEDBACK, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(Feedback feedback : feedbackList.getFeedback() ){
				
				ContentValues cv = new ContentValues();
				cv.put(FeedbackTable.COLUMN_USER_ID, feedback.getUserId().toString());
				cv.put(FeedbackTable.COLUMN_FEEDBACK, feedback.getFeedback());
				cv.put(FeedbackTable.COLUMN_SENT_TIME,feedback.getSentTime());
				
				
			database.insert(FeedbackTable.TABLE_FEEDBACK, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public FeedbackListResultObject getFeedback(){
		
		Log.d(TAG, "Getting feedback");
		
		FeedbackListResultObject feedbackListResObj = new FeedbackListResultObject();
		ArrayList<Feedback> feedback = new ArrayList<Feedback>();
		feedbackListResObj.setFeedback(feedback);
		
		Cursor cursor = database.query(FeedbackTable.TABLE_FEEDBACK, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Feedback fb = cursorToFeedback(cursor);
			feedback.add(fb);
			cursor.moveToNext();
		}
		feedbackListResObj.setFeedback(feedback);
		cursor.close();
		Log.d(TAG, "Feedback count" + feedback.size());
		return feedbackListResObj;
		
	}
	
	private Feedback cursorToFeedback(Cursor cursor){
		
		Feedback ro = new Feedback();
		ro.setId(cursor.getInt(0));
		ro.setUserId(cursor.getString(1));
		ro.setFeedback(cursor.getString(2));
		ro.setSentTime(cursor.getString(3));
		
		return ro;
		
		
	}
	
	
	
}
