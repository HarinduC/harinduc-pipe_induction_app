package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.DocumentTable;
import com.harindu.inductionapp.model.Document;
import com.harindu.inductionapp.model.DocumentListResultObject;


public class DocumentDataSource {

	
private static final String TAG = DocumentDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {DocumentTable.COLUMN_ID, 
			
				DocumentTable.COLUMN_TITLE, 
				DocumentTable.COLUMN_DESCRIPTION,
				DocumentTable.COLUMN_CONTENT};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertAnnouncements(DocumentListResultObject docList) throws SQLException{
		
		Log.d(TAG, "Inserting Documents");
		database.delete(DocumentTable.TABLE_DOCUMENT, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(Document docuemnts : docList.getDocument() ){
				
				ContentValues cv = new ContentValues();
				cv.put(DocumentTable.COLUMN_TITLE, docuemnts.getTitle());
				cv.put(DocumentTable.COLUMN_DESCRIPTION, docuemnts.getDescription());
				cv.put(DocumentTable.COLUMN_CONTENT,docuemnts.getContent());
				
				
			database.insert(DocumentTable.TABLE_DOCUMENT, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public DocumentListResultObject getDocuments(){
		
		Log.d(TAG, "Getting documentss");
		
		DocumentListResultObject documentListResObj = new DocumentListResultObject();
		ArrayList<Document> docs = new ArrayList<Document>();
		documentListResObj.setDocument(docs);
		
		Cursor cursor = database.query(DocumentTable.TABLE_DOCUMENT, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Document documents = cursorToDocuments(cursor);
			docs.add(documents);
			cursor.moveToNext();
		}
		documentListResObj.setDocument(docs);
		cursor.close();
		Log.d(TAG, "Documents count" + docs.size());
		return documentListResObj;
		
	}
	
	private Document cursorToDocuments(Cursor cursor){
		
		Document docs = new Document();
		docs.setId(cursor.getInt(0));
		docs.setTitle(cursor.getString(1));
		docs.setDescription(cursor.getString(2));
		docs.setContent(cursor.getString(3));
		
		return docs;
		
		
	}
	
	
	
	
	
	
}
