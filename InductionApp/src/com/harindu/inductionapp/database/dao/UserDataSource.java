package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.UserTable;
import com.harindu.inductionapp.model.UserModelListResultObject;
import com.harindu.inductionapp.model.UserModel;

public class UserDataSource {

private static final String TAG = UserDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {UserTable.COLUMN_ID, 
			
				UserTable.COLUMN_USER_NAME,
				UserTable.COLUMN_EMAIL,
				UserTable.COLUMN_CATEGORY
				};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertUserGroup(UserModelListResultObject userList) throws SQLException{
		
		Log.d(TAG, "Inserting Users");
		database.delete(UserTable.TABLE_USER, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(UserModel user : userList.getUserModel() ){
				
				ContentValues cv = new ContentValues();
				cv.put(UserTable.COLUMN_USER_NAME, user.getUserName());
				cv.put(UserTable.COLUMN_EMAIL, user.getEmail());
				cv.put(UserTable.COLUMN_CATEGORY,user.getHRISCategory());
				
				database.insert(UserTable.TABLE_USER, null, cv);
				Log.d(TAG, "Saved to database");
					
				}
				database.setTransactionSuccessful();
			}
			finally{
			database.endTransaction();
		}
		
	}
	
	public UserModelListResultObject getUsers(){
		
		Log.d(TAG, "Getting users");
		
		UserModelListResultObject userListResObj = new UserModelListResultObject();
		ArrayList<UserModel> user = new ArrayList<UserModel>();
		userListResObj.setUserModel(user);
		
		
		Cursor cursor = database.query(UserTable.TABLE_USER, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			UserModel u = cursorToUser(cursor);
			user.add(u);
			cursor.moveToNext();
		}
		userListResObj.setUserModel(user);
		cursor.close();
		Log.d(TAG, "Users count" + user.size());
		return userListResObj;
		
	}
	
	private UserModel cursorToUser(Cursor cursor){
		
		UserModel ro = new UserModel();
		ro.setId(cursor.getInt(0));
		ro.setUserName(cursor.getString(1));
		ro.setEmail(cursor.getString(2));
		ro.setHRISCategory(cursor.getString(3));
		return ro;
		
		
	}
}
