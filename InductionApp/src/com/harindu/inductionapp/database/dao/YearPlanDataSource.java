package com.harindu.inductionapp.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.harindu.inductionapp.database.DatabaseHelper;
import com.harindu.inductionapp.database.YearPlanTable;
import com.harindu.inductionapp.model.YearPlan;
import com.harindu.inductionapp.model.YearPlanListResultObject;


public class YearPlanDataSource {

private static final String TAG = YearPlanDataSource.class.getName();
	
	private SQLiteDatabase database ;
	
	private String[] allColumns = {YearPlanTable.COLUMN_ID, 
			
				YearPlanTable.COLUMN_NAME,
				YearPlanTable.COLUMN_DESCRIPTION
				};
	
	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}
	
	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}
	
	public void insertYearPlan(YearPlanListResultObject yearList) throws SQLException{
		
		Log.d(TAG, "Inserting YearPlans");
		database.delete(YearPlanTable.TABLE_YEAR_PLAN, null, null);
		
		try{
			
			database.beginTransaction();
			
			for(YearPlan yearPlan : yearList.getYearPlan() ){
				
				ContentValues cv = new ContentValues();
				cv.put(YearPlanTable.COLUMN_NAME, yearPlan.getName());
				cv.put(YearPlanTable.COLUMN_DESCRIPTION, yearPlan.getDescription());
				
			database.insert(YearPlanTable.TABLE_YEAR_PLAN, null, cv);
			Log.d(TAG, "Saved to database");
				
			}
			database.setTransactionSuccessful();
		}
		finally{
			
			database.endTransaction();
		}
		
	}
	
	public YearPlanListResultObject getYearPlans(){
		
		Log.d(TAG, "Getting user year plans");
		
		YearPlanListResultObject yearPlanListResObj = new YearPlanListResultObject();
		ArrayList<YearPlan> yearPlan = new ArrayList<YearPlan>();
		yearPlanListResObj.setYearPlan(yearPlan);
		
		
		Cursor cursor = database.query(YearPlanTable.TABLE_YEAR_PLAN, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			YearPlan yp = cursorToYearPlan(cursor);
			yearPlan.add(yp);
			cursor.moveToNext();
		}
		yearPlanListResObj.setYearPlan(yearPlan);
		cursor.close();
		Log.d(TAG, "YearPlan count" + yearPlan.size());
		return yearPlanListResObj;
		
	}
	
	private YearPlan cursorToYearPlan(Cursor cursor){
		
		YearPlan ro = new YearPlan();
		ro.setId(cursor.getInt(0));
		ro.setName(cursor.getString(1));
		ro.setDescription(cursor.getString(2));
		return ro;
		
		
	}
}
