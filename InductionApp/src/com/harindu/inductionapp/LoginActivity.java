package com.harindu.inductionapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	EditText etUserName , etPassWord;
	Button btLogin;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		etUserName = (EditText) findViewById(R.id.username);
		etPassWord = (EditText) findViewById(R.id.password);
		btLogin = (Button) findViewById(R.id.login);
		
	}
	
	public void login(View view){
		
		if(etUserName.getText().toString().equals("admin")&&etPassWord.getText().toString().equals("123")){
			
			Intent i = new Intent(LoginActivity.this, MainViewActivity.class);
			startActivity(i);
		}
		
		else{
			
		      Toast.makeText(getApplicationContext(), "Wrong Credentials",Toast.LENGTH_SHORT).show();
		}
		
	}
	
	// Need to implement the AsynLogin class but diidnt implement at this stage 
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
