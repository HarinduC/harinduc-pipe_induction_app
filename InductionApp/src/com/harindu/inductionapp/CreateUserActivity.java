package com.harindu.inductionapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class CreateUserActivity extends Activity {

	EditText firstName,LastName,newUserName,newPassword;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_user);
		
		firstName = (EditText) findViewById(R.id.fname);
		LastName  = (EditText) findViewById(R.id.lname);
		newUserName = (EditText) findViewById(R.id.nusername);
		newPassword = (EditText) findViewById(R.id.npassword);
		
	}
	
	// need to implement the data handling classes where the input data being sent to server and then being used with client for the login purpose
	
	public void create(View view){
		
		Intent i = new Intent(CreateUserActivity.this,LoginActivity.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_user, menu);
		return true;
	}

}
