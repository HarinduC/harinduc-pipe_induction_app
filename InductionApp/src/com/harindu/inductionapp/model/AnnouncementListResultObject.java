package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class AnnouncementListResultObject {

	private ArrayList<Announcements> announcements ;

	public ArrayList<Announcements> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(ArrayList<Announcements> announcements) {
		this.announcements = announcements;
	}
	
	
}
