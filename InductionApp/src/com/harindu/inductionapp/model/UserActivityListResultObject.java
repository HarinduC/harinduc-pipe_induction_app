package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class UserActivityListResultObject {

	private ArrayList<UserActivity> userActivity;

	public ArrayList<UserActivity> getUserActivity() {
		return userActivity;
	}

	public void setUserActivity(ArrayList<UserActivity> userActivity) {
		this.userActivity = userActivity;
	}
	
	
}
