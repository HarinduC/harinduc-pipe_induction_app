package com.harindu.inductionapp.model;

public class UserActivity {

	private int id;
	private String userId;
	private String docId;
	private String annId;
	private String yrId;
	private String fdId;
	private String activity;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getAnnId() {
		return annId;
	}
	public void setAnnId(String annId) {
		this.annId = annId;
	}
	public String getYrId() {
		return yrId;
	}
	public void setYrId(String yrId) {
		this.yrId = yrId;
	}
	public String getFdId() {
		return fdId;
	}
	public void setFdId(String fdId) {
		this.fdId = fdId;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	
}
