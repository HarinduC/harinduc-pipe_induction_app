package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class UserGroupListResultObject {

	private ArrayList<UserGroup> userGroup;

	public ArrayList<UserGroup> getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(ArrayList<UserGroup> userGroup) {
		this.userGroup = userGroup;
	}
	
	
}
