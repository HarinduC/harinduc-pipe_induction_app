package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class UserModelListResultObject {

	private ArrayList<UserModel> userModel;

	public ArrayList<UserModel> getUserModel() {
		return userModel;
	}

	public void setUserModel(ArrayList<UserModel> userModel) {
		this.userModel = userModel;
	}
	
	
}
