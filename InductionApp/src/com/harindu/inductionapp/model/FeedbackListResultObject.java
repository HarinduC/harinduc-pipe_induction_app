package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class FeedbackListResultObject {

	private ArrayList<Feedback> feedback;

	public ArrayList<Feedback> getFeedback() {
		return feedback;
	}

	public void setFeedback(ArrayList<Feedback> feedback) {
		this.feedback = feedback;
	}
	
	
}
