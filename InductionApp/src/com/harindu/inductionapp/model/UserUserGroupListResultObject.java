package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class UserUserGroupListResultObject {

	private ArrayList<UserUserGroup> userUserGroup;

	public ArrayList<UserUserGroup> getUserUserGroup() {
		return userUserGroup;
	}

	public void setUserUserGroup(ArrayList<UserUserGroup> userUserGroup) {
		this.userUserGroup = userUserGroup;
	}
	
	
}
