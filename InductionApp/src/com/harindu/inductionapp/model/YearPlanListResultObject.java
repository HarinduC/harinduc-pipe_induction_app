package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class YearPlanListResultObject {

	private ArrayList<YearPlan> yearPlan;

	public ArrayList<YearPlan> getYearPlan() {
		return yearPlan;
	}

	public void setYearPlan(ArrayList<YearPlan> yearPlan) {
		this.yearPlan = yearPlan;
	}
	
	
}
