package com.harindu.inductionapp.model;

import java.util.ArrayList;

public class DocumentListResultObject {

	private ArrayList<Document> document;

	public ArrayList<Document> getDocument() {
		return document;
	}

	public void setDocument(ArrayList<Document> document) {
		this.document = document;
	}
	
	
}
