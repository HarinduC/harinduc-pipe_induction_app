package com.pipe.induction.network;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.pipe.induction.model.NetworkResultObject;

import android.util.Log;

public class NetworkRequest {
	private static final String TAG = NetworkRequest.class.getSimpleName();

	public NetworkResultObject sendCountyListRequest() {

		NetworkResultObject networkResultObject = new NetworkResultObject();

		HttpClient httpClient = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000);
		HttpResponse response;

		try {
			HttpPost post = new HttpPost("http://api.geonames.org/countryInfoJSON?username=ramindu");
			post.addHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8");
			response = httpClient.execute(post);

			int statusCode = response.getStatusLine().getStatusCode();

			Log.d(TAG, "Status Code : " + statusCode);
			HttpEntity entity = response.getEntity();
			networkResultObject.setResponseString(EntityUtils.toString(entity));
			networkResultObject.setStatusCode(statusCode);
			Log.d(TAG, networkResultObject.getResponseString());

		} catch (Exception e) {
			e.printStackTrace();

		}
		return networkResultObject;

	}

}
