package com.pipe.induction.database.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pipe.induction.database.CountryTable;
import com.pipe.induction.database.DatabaseHelper;
import com.pipe.induction.model.CountryListResultObject;
import com.pipe.induction.model.CountyItemResponseObject;

public class CountryDataSource {

	private static final String TAG = CountryDataSource.class.getName();

	private SQLiteDatabase database;

	private String[] allColumns = { CountryTable.COLUMN_ID,
			CountryTable.COLUMN_COUNTRY_NAME,
			CountryTable.COLUMN_CURRENCY_CODE,
			CountryTable.COLUMN_COUNTRY_CODE, CountryTable.COLUMN_POPULATION };

	public void open(Context context) throws SQLException {
		database = DatabaseHelper.getInstance(context).getWritableDatabase();
	}

	public void close(Context context) {
		DatabaseHelper.getInstance(context).close();
	}

	public void insertCountries(CountryListResultObject countryList)
			throws SQLException {

		Log.d(TAG, "Inserting countries");
		database.delete(CountryTable.TABLE_COUNTRY, null, null);
		try {
			database.beginTransaction();

			for (CountyItemResponseObject country : countryList.getCountryList()) {
				ContentValues contentValues = new ContentValues();
				contentValues.put(CountryTable.COLUMN_COUNTRY_NAME,
						country.getCountryName());
				contentValues.put(CountryTable.COLUMN_CURRENCY_CODE,
						country.getCurrencyCode());
				contentValues.put(CountryTable.COLUMN_COUNTRY_CODE,
						country.getCountryCode());
				contentValues.put(CountryTable.COLUMN_POPULATION,
						country.getPopulation());
				database.insert(CountryTable.TABLE_COUNTRY, null, contentValues);
				Log.d(TAG, "Saved to database");
			}
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
	}

	public CountryListResultObject getCountries() {

		Log.d(TAG, "Getting countries");

		CountryListResultObject countryListResultObject = new CountryListResultObject();
		ArrayList<CountyItemResponseObject> countries = new ArrayList<CountyItemResponseObject>();
		countryListResultObject.setCountyList(countries);

		Cursor cursor = database.query(CountryTable.TABLE_COUNTRY, allColumns,
				null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			CountyItemResponseObject countyItemResponseObject = cursorToCountryItem(cursor);
			countries.add(countyItemResponseObject);
			cursor.moveToNext();
		}
		countryListResultObject.setCountyList(countries);
		cursor.close();
		Log.d(TAG, "Countries count" + countries.size());
		return countryListResultObject;
	}

	private CountyItemResponseObject cursorToCountryItem(Cursor cursor) {
		CountyItemResponseObject responseObject = new CountyItemResponseObject();
		responseObject.setId(cursor.getLong(0));
		responseObject.setCountryName(cursor.getString(1));
		responseObject.setCurrencyCode(cursor.getString(2));
		responseObject.setCountryCode(cursor.getString(3));
		responseObject.setPopulation(cursor.getString(4));
		return responseObject;
	}
}
