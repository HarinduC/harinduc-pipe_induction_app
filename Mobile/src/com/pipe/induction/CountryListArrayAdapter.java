package com.pipe.induction;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pipe.induction.model.CountyItemResponseObject;

public class CountryListArrayAdapter extends BaseAdapter {
	private LayoutInflater inflater = null;
	private ArrayList<CountyItemResponseObject> countyList = null;
	private static final String TAG = CountryListArrayAdapter.class.getSimpleName();

	public CountryListArrayAdapter(ArrayList<CountyItemResponseObject> countyList, Activity activity) {
		inflater = LayoutInflater.from(activity);
		this.countyList = countyList;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView txtCountyName = null;
		TextView txtCountyPopulation = null;
		final CountyItemResponseObject countyItemResponseObject = (CountyItemResponseObject) this.getItem(position);
		// Create a new row view of contacts
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.country_row_view, null);
			// Find the child views.
			txtCountyName = (TextView) convertView.findViewById(R.id.countryName);
			txtCountyPopulation = (TextView) convertView.findViewById(R.id.countryPopulation);
			convertView.setTag(new CountryViewHolder(txtCountyName, txtCountyPopulation));
		}

		CountryViewHolder viewHolder = (CountryViewHolder) convertView.getTag();

		txtCountyName = viewHolder.getTxtCountryName();
		txtCountyPopulation = viewHolder.getTxtCountryPopulation();

		txtCountyName.setText("County Name :" + countyItemResponseObject.getCountryName());
		txtCountyPopulation.setText("County Population :" + countyItemResponseObject.getPopulation());

		return convertView;
	}

	public class CountryViewHolder {

		private TextView txtCountryName;
		private TextView txtCountryPopulation;

		public CountryViewHolder(TextView txtCountryName, TextView txtCountryPopulation) {
			this.txtCountryName = txtCountryName;
			this.txtCountryPopulation = txtCountryPopulation;
		}

		public TextView getTxtCountryName() {
			return txtCountryName;
		}

		public void setTxtCountryName(TextView txtCountryName) {
			this.txtCountryName = txtCountryName;

		}

		public TextView getTxtCountryPopulation() {
			return txtCountryPopulation;
		}

		public void setTxtCountryPopulation(TextView txtCountryPopulation) {
			this.txtCountryPopulation = txtCountryPopulation;
		}

	}

	@Override
	public int getCount() {
		return this.countyList != null ? this.countyList.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		CountyItemResponseObject listItem = null;
		listItem = this.countyList != null ? this.countyList.get(position) : null;
		return listItem;

	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}