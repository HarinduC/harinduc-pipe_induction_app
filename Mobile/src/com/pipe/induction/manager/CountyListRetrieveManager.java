package com.pipe.induction.manager;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.pipe.induction.database.dao.CountryDataSource;
import com.pipe.induction.model.CountryListResultObject;
import com.pipe.induction.model.CountyItemResponseObject;
import com.pipe.induction.model.NetworkResultObject;
import com.pipe.induction.network.NetworkRequest;

public class CountyListRetrieveManager {
	private static final String TAG = CountyListRetrieveManager.class
			.getSimpleName();
	private NetworkResultObject networkResultObject;
	private CountryListResultObject countryListResultObject;
	private Context context;

	public void performAction(Context context) {
		Log.d(TAG, "performAction");
		this.context = context;
		countryListResultObject = new CountryListResultObject();
		new RetreiveCountyListTask()
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	class RetreiveCountyListTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			Boolean result = false;
			NetworkRequest loginRequest = new NetworkRequest();
			CountyListRetrieveManager.this.networkResultObject = loginRequest
					.sendCountyListRequest();
			;
			result = handleResultObject(CountyListRetrieveManager.this.networkResultObject);
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				Log.d(TAG, "OK");
				Log.d(TAG, "County list size :"
						+ countryListResultObject.getCountryList().size());
				for (CountyItemResponseObject obj : countryListResultObject
						.getCountryList()) {
					Log.d(TAG, "County Name : " + obj.getCountryName());
				}

				// TODO :Save to database and sent broadcast intent
				if (countryListResultObject.getCountryList() != null
						&& !countryListResultObject.getCountryList().isEmpty()) {

					CountryDataSource countryDataSource = new CountryDataSource();
					countryDataSource.open(context);
					countryDataSource.insertCountries(countryListResultObject);

					Log.d(TAG, "Data Count"
							+ countryDataSource.getCountries().getCountryList()
									.size());
					countryDataSource.close(context);
				}
			} else {
				Log.d(TAG, "NO");
			}

			sentBroadcastIntent(result);
		}
	}

	public boolean handleResultObject(NetworkResultObject networkResultObject) {
		boolean isSucess = false;

		if (networkResultObject != null) {
			if (networkResultObject.getStatusCode() == 200) {
				Gson gson = new Gson();
				try {

					CountryListResultObject obj = gson.fromJson(
							networkResultObject.getResponseString(),
							CountryListResultObject.class);
					countryListResultObject.setCountyList(obj.getCountryList());
					// Success
					isSucess = true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
		return isSucess;
	}

	private void sentBroadcastIntent(boolean result) {
		Intent i = new Intent("COUNTRY_LIST_RETRIEVE_BROADCAST");
		i.putExtra("COUNTRY_LIST_RETRIEVE_RESULT", result);

		context.sendBroadcast(i);
	}

}
