package com.pipe.induction;

import java.io.File;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.pipe.induction.database.dao.CountryDataSource;
import com.pipe.induction.manager.CountyListRetrieveManager;
import com.pipe.induction.model.CountryListResultObject;

public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();
	private ActionBar actionBar;
	private ProgressDialog progress;
	private ListView countryListView;
	private static int TAKE_PICTURE = 1;
	private Uri imageUri;

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "County list retrieve broadcast rceived.");
			handleBrodcastIntentData(intent);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");
		setContentView(R.layout.activity_main);

		IntentFilter intentFilter = new IntentFilter("COUNTRY_LIST_RETRIEVE_BROADCAST");
		// registering our receiver
		this.registerReceiver(mReceiver, intentFilter);

		actionBar = getActionBar();

		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("PIPE V2");
		// actionBar.se

		countryListView = (ListView) findViewById(R.id.countryList);

	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		this.unregisterReceiver(this.mReceiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onDestroy");
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.save_settings:
			Log.d(TAG, "Click on SAVE");
			break;
		case android.R.id.home:
			finish();
			break;
		}

		return true;

	}

	public void clickOnRetrieveCountyList(View view) {
		progress = ProgressDialog.show(this, "Wait", "Loading....", true);
		CountyListRetrieveManager manager = new CountyListRetrieveManager();
		manager.performAction(getApplicationContext());
	}

	public void clickOnEmail(View view) {
		Log.d(TAG, "clickOnEmail");

	}

	public void clickOnPhone(View view) {
		Log.d(TAG, "clickOnPhone");
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + "0785666145"));
		this.startActivity(intent);
	}

	public void clickOnCamera(View view) {
		Log.d(TAG, "clickOnCamera");

		Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
		File photo = new File(Environment.getExternalStorageDirectory(), "PicUomIt.jpg");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
		imageUri = Uri.fromFile(photo);
		startActivityForResult(intent, TAKE_PICTURE);
	}

	public void clickOnEdit(View view) {
		Log.d(TAG, "clickOnEdit");
	}

	private void handleBrodcastIntentData(Intent intent) {
		boolean result = intent.getBooleanExtra("COUNTRY_LIST_RETRIEVE_RESULT", false);
		progress.dismiss();
		if (result) {
			CountryDataSource countryDataSource = new CountryDataSource();
			countryDataSource.open(this);
			CountryListResultObject countryListResultObject = countryDataSource.getCountries();
			countryDataSource.close(this);
			if (countryListResultObject.getCountryList() != null) {

				CountryListArrayAdapter countryListArrayAdapter = new CountryListArrayAdapter(countryListResultObject.getCountryList(), MainActivity.this);
				countryListView.setAdapter(countryListArrayAdapter);
			}

		} else {
			// Show error message
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Error").setMessage("List retrirve error").setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
}
