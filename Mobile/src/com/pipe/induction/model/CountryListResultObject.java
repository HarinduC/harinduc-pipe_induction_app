package com.pipe.induction.model;

import java.util.ArrayList;


public class CountryListResultObject {

	private ArrayList<CountyItemResponseObject> geonames;

	public ArrayList<CountyItemResponseObject> getCountryList() {
		return geonames;
	}

	public void setCountyList(ArrayList<CountyItemResponseObject> countyList) {
		this.geonames = countyList;
	}
}
